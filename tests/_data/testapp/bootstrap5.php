<?php
/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
ini_set('display_errors', 1);
require dirname(__DIR__, 3).'/vendor/autoload.php';
require __DIR__.'/lib/Component.php';
require __DIR__.'/lib/Asset.php';
$languageSwitcher = new \uhi67\languageswitcher\LanguageSwitcher([
	'language' => 'en-GB',
	'languages' => ['en-GB' => 'English', 'hu-HU' => 'Magyar'],
	'callback' => function($la) {
		// optional: save selected language into user record
	}
]);

$languageswitcherAsset = new \_data\testapp\lib\Asset([
	'path' => '../../src/assets',
	'patterns' => [
		'languageswitcher.css',
		'flags/flags.min.css',
		'flags/flags.png',
		'flags/blank.gif'
	]
]);

?>
<html lang="en">
	<head>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- CSS only -->
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
		<!-- Link languageSwitcher assets using any asset manager -->
		<link rel="stylesheet" type="text/css" href="<?= $languageswitcherAsset->url('flags/flags.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= $languageswitcherAsset->url('languageswitcher.css') ?>">
		<title>Bootstrap 5 -- LanguageSwitcher demo</title>
	</head>
	<body>
		<h1>LanguageSwitcher demo</h1>
		<h2>Bootstrap 5 version</h2>
		<nav class="navbar navbar-light">
			<ul id="w1" class="nav">
				<li class="nav-item"><a class="nav-link" href="/">Back to main menu</a></li>
				<li class="nav-item dropdown">
					<a href="#" class="nav-link dropdown-toggle" data-bs-toggle="dropdown">Dropdown</a>
					<ul class="dropdown-menu">
						<li><a class="dropdown-item" href="#">Action</a></li>
						<li><a class="dropdown-item" href="#">Another action</a></li>
						<li><a class="dropdown-item disabled" href="#">Something else here</a></li>
					</ul>
				</li>
				<?= $languageSwitcher->items(['bsVer'=>5]) ?>
			</ul>
		</nav>
		<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
	</body>
</html>
