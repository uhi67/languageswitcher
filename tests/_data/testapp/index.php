<html lang="en">
	<head>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">
		<title>LanguageSwitcher demo</title>
	</head>
<body>
	<h1>LanguageSwitcher demo</h1>
	<nav class="navbar navbar-default">
		<ul id="w1" class="nav nav-pills ">
			<li class=""><a href="bootstrap3.php">Bootstrap 3 test</a></li>
			<li class=""><a href="bootstrap4.php">Bootstrap 4 test</a></li>
			<li class=""><a href="bootstrap5.php">Bootstrap 5 test</a></li>
		</ul>
	</nav>
</body>
</html>
