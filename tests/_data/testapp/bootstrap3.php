<?php
/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
ini_set('display_errors', 1);
require dirname(__DIR__, 3).'/vendor/autoload.php';
require __DIR__.'/lib/Component.php';
require __DIR__.'/lib/Asset.php';
$languageSwitcher = new \uhi67\languageswitcher\LanguageSwitcher([
	'language' => 'en-GB',
	'languages' => ['en-GB' => 'English', 'hu-HU' => 'Magyar'],
	'callback' => function($la) {
		// optional: save selected language into user record
	}
]);

$languageswitcherAsset = new \_data\testapp\lib\Asset([
	'path' => '../../src/assets',
	'patterns' => [
		'languageswitcher.css',
		'flags/flags.min.css',
		'flags/flags.png',
		'flags/blank.gif'
	]
]);

?>
<html lang="en">
	<head>
		<!-- Latest compiled and minified CSS -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap.min.css" integrity="sha384-HSMxcRTRxnN+Bdg0JdbxYKrThecOKuH5zCYotlSAcp1+c8xmyTe9GYg1l9a69psu" crossorigin="anonymous">

		<!-- Optional theme -->
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/css/bootstrap-theme.min.css" integrity="sha384-6pzBo3FDv/PJ8r2KRkGHifhEocL+1X2rVCTTkUfGk7/0pbek5mMa1upzvWbrUbOZ" crossorigin="anonymous">

		<!-- Link languageSwitcher assets using any asset manager -->
		<link rel="stylesheet" type="text/css" href="<?= $languageswitcherAsset->url('flags/flags.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= $languageswitcherAsset->url('languageswitcher.css') ?>">

		<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>

		<!-- Latest compiled and minified JavaScript -->
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@3.4.1/dist/js/bootstrap.min.js" integrity="sha384-aJ21OjlMXNL5UyIl/XNwTMqvzeRMZH2w8c5cRVpzpU8Y5bApTppSuUkhZXN0VxHd" crossorigin="anonymous"></script>
		<title>Bootstrap 3 -- LanguageSwitcher demo</title>
	</head>
	<body>
		<h1>LanguageSwitcher demo</h1>
		<h2>Bootstrap 3 version</h2>
		<nav class="navbar navbar-default">
			<ul id="w1" class="nav nav-pills ">
				<li><a href="/">Back to main menu</a></li>
				<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">Dropdown <span class="caret"></span></a>
					<ul class="dropdown-menu">
						<li><a href="#">Action</a></li>
						<li><a href="#">Another action</a></li>
						<li class="disabled"><a href="#">Something else here</a></li>
					</ul>
				</li>
				<?= $languageSwitcher->items(['bsVer'=>3]) ?>
			</ul>
		</nav>
	</body>
</html>
