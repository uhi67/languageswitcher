<?php
/** @noinspection PhpUnhandledExceptionInspection */
/** @noinspection PhpFullyQualifiedNameUsageInspection */
ini_set('display_errors', 1);
require dirname(__DIR__, 3).'/vendor/autoload.php';
require __DIR__.'/lib/Component.php';
require __DIR__.'/lib/Asset.php';
$languageSwitcher = new \uhi67\languageswitcher\LanguageSwitcher([
	'language' => 'en-GB',
	'languages' => ['en-GB' => 'English', 'hu-HU' => 'Magyar'],
	'callback' => function($la) {
		// optional: save selected language into user record
	}
]);

$languageswitcherAsset = new \_data\testapp\lib\Asset([
	'path' => '../../src/assets',
	'patterns' => [
		'languageswitcher.css',
		'flags/flags.min.css',
		'flags/flags.png',
		'flags/blank.gif'
	]
]);

?>
<html lang="en">
	<head>
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">

		<!-- Link languageSwitcher assets using any asset manager -->
		<link rel="stylesheet" type="text/css" href="<?= $languageswitcherAsset->url('flags/flags.min.css') ?>">
		<link rel="stylesheet" type="text/css" href="<?= $languageswitcherAsset->url('languageswitcher.css') ?>">

		<!-- JQuery must be loaded separately -->
		<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>

		<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
		<title>Bootstrap 4 -- LanguageSwitcher demo</title>
	</head>
	<body>
		<h1>LanguageSwitcher demo</h1>
		<h2>Bootstrap 4 version</h2>
		<nav class="navbar navbar-light">
			<ul id="w1" class="nav">
				<li class="nav-item"><a class="nav-link" href="/">Back to main menu</a></li>
				<li class="nav-item dropdown">
					<a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown">Dropdown</a>
					<ul class="dropdown-menu">
						<li><a class="dropdown-item" href="#">Action</a></li>
						<li><a class="dropdown-item" href="#">Another action</a></li>
						<li><a class="dropdown-item disabled" href="#">Something else here</a></li>
					</ul>
				</li>
				<?= $languageSwitcher->items(['bsVer'=>4]) ?>
			</ul>
		</nav>
	</body>
</html>
