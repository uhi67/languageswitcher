Language Switcher
=================

### Version 1.1 -- 2022-10-24

A simple Language Switcher component, designed for boostrap navbar use.
No widget, no ajax, no additional javascript.

Prerequisites
-------------

    php >= 5.6
    Bootstrap 3,4,5 -- bootstrap is not explicitly required, feel free to include any version in the top-level project
    popper.js (1.x for Bootstrap 3/4, 2.x for Bootstrap 5)
    jQuery (for Bootstrap 3/4, optional for Bootstrap 5)

Installation
------------

The preferred way to install this extension is through composer.

To install, either run

    composer require uhi67/languageswitcher "1.*" 

or add

    "uhi67/languageswitcher" : "^1.*"

or clone form github

    git clone https://github.com/uhi67/languageswitcher

Usage
-----

### Settings in component creation

Define selectable languages using canonical `ll-CC` notation. (However it works pretty well with language codes only)
An optional callback may be defined to perform on language selection, e.g. to save the language into the user record.  

```php
    $languageSwitcher = new \uhi67\languageswitcher\LanguageSwitcher([
        'languages' => ['en-GB' => 'English', 'hu-HU' => 'Magyar'],
        'callback' => function($la) {
            // optional: save selected language into user record
        }
    ]);
```
    
See all configuration options at class source. 

### Usage in views

Register the following assets:

- `/vendor/uhi67/languageswitcher/src/assets/flags/flags.min.css`
- `/vendor/uhi67/languageswitcher/src/assets/languageswitcher.css`

Then render the bootstrap nav:

```html
    <ul id="w1" class="nav">
        <?= $languageSwitcher->items() ?>
    </ul>
```

All bootstrap version should work without specifying the version. However, if any compatibility issue occurs, you can 
render with explicit Boostrap version to reduce class declarations not needed for your version:

```html
    <ul id="w1" class="nav">
        <?= $languageSwitcher->items(['bsVer'=>5]) ?>
    </ul>
```

See also the test application in `test/_data/testapp` directory.

## Changes

### v1.1 -- 2022-10-24

- Bootstrap 5 compatibility
- Examples

### v1.0.1 -- 2022-09-30

- phpdoc fix: items() returns string

### v1.0 -- 2020-11-17

- frameworkless version (based on https://bitbucket.org/uhi67/yii2-languageswitcher)
