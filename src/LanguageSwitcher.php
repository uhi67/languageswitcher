<?php /** @noinspection PhpUnused */

namespace uhi67\languageswitcher;

/**
 * Class LanguageSwitcher
 *
 * Configuration parameters:
 * - languages -- set of available languages e.g. ['hu'=>'Magyar', ...]
 * - callback -- user callback on setting language function($la)
 * - language -- currently selected language (initialize with default language)
 * - translator -- function($message, $params, $language)
 * - cookie -- cookie name
 * - session -- session variable name
 * - cookieExpire -- in seconds (default is 30 days)
 * - cookieDomain -- (default is true == own domain)
 * @package uhi67\languageswitcher
 */
class LanguageSwitcher extends BaseObject {
	/** @var array $languages -- set of available languages e.g. ['hu'=>'Magyar', ...] */
	public $languages;
	/** @var callable $callback -- user callback on setting language function($la) */
	public $callback;
	/** @var string $language -- currently selected language (initialize with default language) */
	public $language;
	/** @var callable $translator -- function($message, $params, $language) */
	public $translator = null;
	public $cookie;
	public $cookieExpire = 3600 * 24 * 30;
	public $cookieDomain = true;
	public $session;

	/**
	 * @var array countries
	 * Maps ISO 639-1 languages (without locale) to ISO 3166-1 country code
	 */
	public static $countries = [
		'en' => 'gb',
	];

	public function init() {
		// Language switch
		if($this->cookieDomain === true) $this->cookieDomain = $_SERVER['HTTP_HOST'];
		if($la = AppHelper::getReq('la')) {
			if($this->session) $_SESSION[$this->session] = $this->language = $la;
			if($this->cookie) setcookie($this->cookie, $this->language = $la, time() + $this->cookieExpire, '/', $this->cookieDomain);
			if(is_callable($this->callback)) call_user_func($this->callback, $la);
			AppHelper::redirect(AppHelper::createUrl(['la' => null]));
			return;
		}
		$la = AppHelper::getSession('la');
		if($la) $this->language = $la;
	}

	/**
	 * Returns an item list html for Bootstrap NavBar
	 *
	 * Usage
	 *
	 * ```
	 *	<nav ...>
	 *		<ul id="w1" class="navbar-nav navbar-right nav">
 *				<?= $languageSwitcher->items() ?>
	 *		</ul>
	 *	</nav>
	 * ```
	 *
	 * Display options:
	 * - currentName (bool) -- display current language name in language menu item (default is true)
	 * - itemName (bool)  -- display language name in selectable items (default is true)
	 * - title (string) -- default is 'Select language' (translated to current language)
	 * - flag (bool) -- display flag (default is true)
	 * - country (bool) -- display country code with language code when no flag is displayed (default is false)
	 * - bsVer -- Bootstrap version. Default is empty, creating an universal output. You can also specify 3,4,5
	 *
	 * @param array $options -- display options
	 *
	 * @return string
	 */
	public function items($options = ['flag'=>true]) {
		$languages = $this->languages;
		$currentName = AppHelper::getValue($options, 'currentName', true);
		$itemName = AppHelper::getValue($options, 'itemName', true);
		$showFlag = AppHelper::getValue($options, 'flag', true);
		$country = AppHelper::getValue($options, 'country', false);
		$bsVer = AppHelper::getValue($options, 'bsVer', false);

		$lang = $this->language;
		$countryCode = static::country($lang);
		$langName = AppHelper::getValue($languages, $lang, $lang);

		$items = array_map(
			function($la, $language) use($languages, $itemName, $showFlag, $country, $bsVer) {
				$caption = $itemName ? $language : '';
				$prefix = $showFlag ? '<i class="'.'flag flag-'.static::country($la).'"></i>' : strtoupper($country ? $la : substr($la,0,2));
				$dash = $caption && !$showFlag ? ' - ' : '';
				$enabled = $la != $this->language && AppHelper::isGet();
				$label = $prefix.$dash.$caption;
				$url = $enabled ? AppHelper::createUrl(['la'=>$la]) : '';
				$itemOptions = [];
				$linkOptions = ['href'=>$url];
				if(!$bsVer || $bsVer>3) $linkOptions['class'] = 'dropdown-item';
				if(!$enabled) {
					$itemOptions['class'] = 'disabled';
					if(isset($linkOptions['class'])) $linkOptions['class'] .= ' disabled';
					else $linkOptions['class'] = 'disabled';
				}
				$a = AppHelper::tag('a', $label, $linkOptions);
				return AppHelper::tag('li', $a, $itemOptions);

			},
			array_keys($this->languages),
			array_values($this->languages)
		);

		$label = ($showFlag ? '<i class="'.'flag flag-'.$countryCode.'"></i>':'') .
			($currentName ? $langName : '') .
			((!$currentName && !$showFlag) ? strtoupper($country ? $lang : substr($lang,0,2)) : '');
		if(!$bsVer || $bsVer==3) $label .= AppHelper::tag('span', '', ['class'=>'caret']);
		$toggleData = [(!$bsVer || $bsVer<5 ? 'toggle' : 'bs-toggle') => 'dropdown']; if(!$bsVer) $toggleData['bs-toggle'] = 'dropdown';
		$aClass = 'dropdown-toggle' . ((!$bsVer || $bsVer>3) ? ' nav-link' : '');
		$toggle = AppHelper::tag('a', $label, ['class' => $aClass, 'data' => $toggleData]);
		$dropdown = AppHelper::tag('ul', $items, ['class'=>'dropdown-menu']);
		$liClass = 'language-switcher dropdown'; if(!$bsVer || $bsVer>3) $liClass .= ' nav-item';
		return AppHelper::tag('li', $toggle . $dropdown, ['class'=>$liClass]);
	}

	/**
	 * Determines ISO 3166-1 location code of an ISO/IEC 15897 locale or ISO 639-1 language code.
	 * If explicit mapping is not found, returns the alpha-2 language code.
	 * @param string $locale
	 * @return string -- the alpha-2 country code in lowercase
	 */
	public static function country($locale) {
		if(strlen($locale)==5) return strtolower(substr($locale,3));
		return AppHelper::getValue(static::$countries, $locale, $locale);
	}

	public function t($message, $params = [], $language = null) {
		if(!$language) $language = $this->language;
		if(is_callable($this->translator)) return call_user_func($this->translator, $message, $params, $language);
		return $message;
	}
}
