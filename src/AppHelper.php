<?php /** @noinspection PhpUnused */

namespace uhi67\languageswitcher;

class AppHelper {
	/**
	 * Retrieves the value of an array element with the given key.
	 * If the key does not exist in the array, the default value will be returned instead.
	 *
	 * @param array $array array or object to extract value from
	 * @param string|array $key key name of the array element or an array of keys
	 * @param mixed $default the default value to be returned if the specified array key does not exist.
	 * @return mixed the value of the element if found, default value otherwise
	 */
	public static function getValue($array, $key, $default = null) {
		if (is_array($key)) {
			$lastKey = array_pop($key);
			foreach ($key as $keyPart) {
				$array = static::getValue($array, $keyPart);
			}
			$key = $lastKey;
		}

		if (is_array($array) && (isset($array[$key]) || array_key_exists($key, $array))) {
			return $array[$key];
		}

		if (is_array($array)) {
			return (isset($array[$key]) || array_key_exists($key, $array)) ? $array[$key] : $default;
		}

		return $default;
	}

	/**
	 * Creates a URL by using the current route and the GET parameters.
	 *
	 * You may modify or remove some of the GET parameters, or add additional query parameters through
	 * the `$params` parameter.
	 *
	 * @param array $params an associative array of parameters that will be merged with the current GET parameters.
	 * If a parameter value is null, the corresponding GET parameter will be removed.
	 * @return string the generated URL
	 */
	public static function createUrl(array $params = []) {
		$params = array_replace_recursive(self::getQueryParams(), $params);
		$path = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
		return $path.'?'.http_build_query($params);
	}

	public static function getQueryParams() {
		return $_GET;
	}

	public static function getReq($name, $default=null) {
		return isset($_REQUEST[$name]) ? $_REQUEST[$name] : $default;
	}

	public static function getSession($name, $default=null) {
		return isset($_SESSION[$name]) ? $_SESSION[$name] : $default;
	}

	public static function isGet() {
		return $_SERVER['REQUEST_METHOD'] === 'GET';
	}

	public static function redirect($location) {
		header ("Location: $location"); /* Redirect browser */
		echo <<<EOT
	<html lang="hu">
	<head>
		<title>Redirect</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">	
	</head>
	<body>
		<script type="text/javascript">location.href='$location';</script>
		<p><a href="$location">[Tovább]</a></p>
	</body>
	</html>
EOT;
	}

	/**
	 * Generates an unordered list.
	 * @param array $items the items for generating the list. Each item generates a single list item.
	 * Note that items will be automatically HTML encoded if `$options['encode']` is not set or true.
	 * @param array $options options (name => config) for the radio button list. The following options are supported:
	 * - itemOptions: array, the HTML attributes for the `li` tags. This option is ignored if the `item` option is specified.
	 * @return string the generated unordered list. An empty list tag will be returned if `$items` is empty.
	 */
	public static function ul($items, $options = []) {
		$itemOptions = AppHelper::fetch($options, 'itemOptions', []);

		if (empty($items)) {
			return static::tag('ul', '', $options);
		}

		$results = [];
		foreach ($items as $item) {
			$results[] = static::tag('li', $item, $itemOptions);
		}

		return static::tag(
			'ul',
			"\n" . implode("\n" , $results) . "\n",
			$options
		);
	}

	public static function fetch(&$array, $name, $default) {
		if(array_key_exists($name, $array)) {
			$value = $array[$name];
			unset($array[$name]);
		}
		else $value = $default;
		return $value;
	}

	/**
	 * @param string $name
	 * @param string $content
	 * @param array $options
	 * @return string
	 */
	public static function tag($name, $content = '', $options = []) {
		if(is_array($content)) $content = implode('', $content);
		if ($name === null || $name === false) {
			return $content;
		}
		$html = "<$name" . static::renderTagAttributes($options) . '>';
		return "$html$content</$name>";
	}

	public static function renderTagAttributes($attributes) {
		$html = '';
		foreach ($attributes as $name => $value) {
			if (is_bool($value)) {
				if ($value) {
					$html .= " $name";
				}
			} elseif (is_array($value)) {
				if($name === 'data') {
					foreach($value as $n => $v) $html .= " data-$n=\"$v\"";
				}
				else { // including class
					if (empty($value)) {
						continue;
					}
					$html .= " $name=\"" . implode(' ', $value) . '"';
				}
			} elseif ($value !== null) {
				$html .= " $name=\"" . $value . '"';
			}
		}
		return $html;
	}

	/**
	 * Converts strings like 'tag1 tag2', 'tag1-tag2' or 'tag1_tag2' to format 'Tag1Tag2'
	 *
	 * @param string $name
	 *
	 * @return string
	 */
	public static function camelize($name) {
		return implode('', array_map(function($e) {
			return ucfirst($e);
		}, preg_split('/[\s_-]+/', $name)));
	}

}
